# ComMan

ConMan is an API that will allow you to manage your email communication for all your applications. Thus making it easier to maintain your email server, to track the status of an email (queued, sending, sent, error), and more in the future

## Getting started

### Parameters

| Env               | Value                                          | Description                                                      |
| ----------------- | ---------------------------------------------- | ---------------------------------------------------------------- |
| DEBUG             | `true`/`false` (default: `false`)              | Set the application in debug mode                                |
| ALLOWED_HOSTS     | `string` (default: `localhost`)                | Set the domains allowed to access the API (comma separated list) |
| SECRET_KEY        | `string` (required)                            | Set the Django secret key                                        |
| DB_NAME           | `string` (default: `comman`)                   | Name of the database to use                                      |
| DB_USER           | `string` (default: `comman`)                   | Username to connect to the PostgreSQL database                   |
| DB_PASSWORD       | `string` (default: `comman`)                   | Password to connect to the PostgreSQL database                   |
| DB_HOST           | `string` (default: `localhost`)                | Host or IP of the PostgreSQL database                            |
| DB_PORT           | `int` (default: `5432`)                        | Port of the PostgreSQL database                                  |
| CELERY_BROKER_URL | `string` (default: `redis://localhost:6379/0`) | URL to the Redis server for Celery                               |
| EMAIL_HOST        | `string` (default: `localhost`)                | Host or IP of the SMTP server                                    |
| EMAIL_PORT        | `int` (default: `587`)                         | TLS port of the SMTP server                                      |
| EMAIL_USER        | `string`                                       | Username to log into the SMTP server                             |
| EMAIL_PASSWORD    | `string`                                       | Password to log into the SMTP server                             |

### Start the project

```bash
git clone git@gitlab.com:HeavenSleep/comman.git
cd conman
pip install -r requirements.txt
src/manage.py migrate
src/manage.py runserver
```

### Start the worker

```bash
cd src/
celery -A comman worker --loglevel info
```

### Create superuser and add allowed emails

```bash
src/manage.py createsuperuser
```

Go to the [Admin page](http://localhost:8000/admin/) and add a new allowed email for your user.

## Usage

See [Swagger](http://localhost:8000/swagger/) or [ReDoc](http://localhost:8000/redoc/) after starting the server.

## Contributing

See [CONTRIBUTING](CONTRIBUTING)

## Authors and acknowledgment
Benjamin Schwald - Author

## License
MPL 2.0 (Mozilla Public License Version 2.0)

## Project status
Beta

from django.contrib.auth.models import User
import uuid
from django.db import models

class Email(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    sender = models.ForeignKey(User, on_delete=models.CASCADE, related_name='sent_emails')
    subject = models.CharField(max_length=255)
    body = models.TextField()
    from_email = models.EmailField()
    from_name = models.CharField(max_length=255, blank=True, null=True)
    to_email = models.EmailField()
    scheduled_time = models.DateTimeField(null=True, blank=True)
    status = models.CharField(max_length=20, default='queued')
    error_message = models.TextField(null=True, blank=True)
    queued_at = models.DateTimeField(auto_now_add=True)
    sent_at = models.DateTimeField(null=True, blank=True)
    retry_count = models.IntegerField(default=0)
    
    def __str__(self) -> str:
        return f"{self.from_email} - {self.subject}"

class AllowedEmailSender(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='allowed_senders')
    from_email = models.EmailField()
    from_name = models.CharField(max_length=255, blank=True, null=True)
    
    def __str__(self) -> str:
        return f"[{self.user}] {self.from_name} <{self.from_email}>"

from drf_yasg.utils import swagger_auto_schema
from drf_yasg import openapi
from rest_framework import serializers, status, viewsets
from rest_framework_simplejwt.views import (
    TokenBlacklistView,
    TokenObtainPairView,
    TokenRefreshView,
    TokenVerifyView,
)
from rest_framework import serializers
from rest_framework.permissions import IsAuthenticated
from rest_framework.decorators import action
from rest_framework.response import Response


from .models import Email, AllowedEmailSender
from .serializers import EmailSerializer, EmailCreateSerializer
from .tasks import send_async_email

RETRY_RESPONSE = openapi.Response('Email retry', {'status': ''})

class EmailViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated]
    queryset = Email.objects.none()

    def get_queryset(self):
        if getattr(self, 'swagger_fake_view', False):
            return Email.objects.all()

        user = self.request.user
        if user.is_superuser:
            return Email.objects.all()
        return Email.objects.filter(sender=user)
    
    def get_serializer_class(self):
        if getattr(self, 'swagger_fake_view', False):
            return EmailSerializer
        if self.action == 'create':
            return EmailCreateSerializer
        return EmailSerializer
    
    @swagger_auto_schema(
        request_body=EmailCreateSerializer,
        responses={
            status.HTTP_201_CREATED: EmailSerializer
        }
    )
    def create(self, request, *args, **kwargs):
        # Use the EmailCreateSerializer for validating the input
        serializer = EmailCreateSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        # Actually create the object using perform_create
        self.perform_create(serializer)

        # Use the EmailSerializer for the response
        response_serializer = EmailSerializer(instance=serializer.instance)
        
        return Response(response_serializer.data, status=status.HTTP_201_CREATED)

    def perform_create(self, serializer):
        user = self.request.user
        from_email = self.request.data.get('from_email')
        from_name = self.request.data.get('from_name')

        # Check if the user is allowed to use this 'from_email'
        if not AllowedEmailSender.objects.filter(user=user, from_email=from_email, from_name=from_name).exists():
            raise serializers.ValidationError("You are not allowed to send email with this identity.")

        email = serializer.save(sender=user)

        if email.scheduled_time:
            send_async_email.apply_async(
                (email.id,), eta=email.scheduled_time)

        if not email.scheduled_time:
            send_async_email.delay(email.id)
    
    @swagger_auto_schema(
        responses={
            status.HTTP_200_OK: 'Email updated',
            status.HTTP_403_FORBIDDEN: 'Only superusers can update emails'
        }
    )
    def update(self, request, *args, **kwargs):
        user = self.request.user
        if not user.is_superuser:
            return Response({'status': 'Only superusers can update emails.'}, status=status.HTTP_403_FORBIDDEN)
        return super().update(request, *args, **kwargs)
    
    @swagger_auto_schema(
        responses={
            status.HTTP_200_OK: 'Email updated',
            status.HTTP_403_FORBIDDEN: 'Only superusers can update emails'
        }
    )
    def partial_update(self, request, *args, **kwargs):
        user = self.request.user
        if not user.is_superuser:
            return Response({'status': 'Only superusers can patch emails.'}, status=status.HTTP_403_FORBIDDEN)
        return super().update(request, *args, **kwargs)

    @swagger_auto_schema(
        responses={
            status.HTTP_204_NO_CONTENT: 'Email deleted',
            status.HTTP_403_FORBIDDEN: 'Only superusers can update emails'
        }
    )
    def destroy(self, request, *args, **kwargs):
        user = self.request.user
        if not user.is_superuser:
            return Response({'status': 'Only superusers can delete emails.'}, status=status.HTTP_403_FORBIDDEN)
        return super().destroy(request, *args, **kwargs)

    @swagger_auto_schema(
        responses={
            status.HTTP_200_OK: 'Email has been requeued for sending',
            status.HTTP_400_BAD_REQUEST: 'Email is not in an error state',
            status.HTTP_403_FORBIDDEN: 'You do not have permission to retry this email'
        }
    )
    @action(detail=True, methods=['GET'])
    def retry(self, request, pk=None):
        email = self.get_object()
        
        # Permissions check
        if request.user != email.sender and not request.user.is_superuser:
            return Response({'status': 'You do not have permission to retry this email.'}, status=status.HTTP_403_FORBIDDEN)

        # Check if the email status is 'error' before retrying
        if email.status != 'error':
            return Response({'status': 'Email is not in an error state.'}, status=status.HTTP_400_BAD_REQUEST)

        # Update the status and retry count (if you've added retry_count)
        email.status = 'queued'
        email.retry_count += 1
        email.save()

        # Trigger the email sending task again
        send_async_email.apply_async((email.id,), eta=email.scheduled_time)

        return Response({'status': 'Email has been requeued for sending.'}, status=status.HTTP_200_OK)


class TokenObtainPairResponseSerializer(serializers.Serializer):
    access = serializers.CharField()
    refresh = serializers.CharField()

    def create(self, validated_data):
        raise NotImplementedError()

    def update(self, instance, validated_data):
        raise NotImplementedError()


class DecoratedTokenObtainPairView(TokenObtainPairView):
    @swagger_auto_schema(
        responses={
            status.HTTP_200_OK: TokenObtainPairResponseSerializer,
            status.HTTP_401_UNAUTHORIZED: 'No active account found with the given credentials'
        }
    )
    def post(self, request, *args, **kwargs):
        return super().post(request, *args, **kwargs)


class TokenRefreshResponseSerializer(serializers.Serializer):
    access = serializers.CharField()

    def create(self, validated_data):
        raise NotImplementedError()

    def update(self, instance, validated_data):
        raise NotImplementedError()


class DecoratedTokenRefreshView(TokenRefreshView):
    @swagger_auto_schema(
        responses={
            status.HTTP_200_OK: TokenRefreshResponseSerializer,
            status.HTTP_401_UNAUTHORIZED: 'Refresh token has expired'
        }
    )
    def post(self, request, *args, **kwargs):
        return super().post(request, *args, **kwargs)


class TokenVerifyResponseSerializer(serializers.Serializer):
    def create(self, validated_data):
        raise NotImplementedError()

    def update(self, instance, validated_data):
        raise NotImplementedError()


class DecoratedTokenVerifyView(TokenVerifyView):
    @swagger_auto_schema(
        responses={
            status.HTTP_200_OK: TokenVerifyResponseSerializer,
        }
    )
    def post(self, request, *args, **kwargs):
        return super().post(request, *args, **kwargs)


class TokenBlacklistResponseSerializer(serializers.Serializer):
    def create(self, validated_data):
        raise NotImplementedError()

    def update(self, instance, validated_data):
        raise NotImplementedError()


class DecoratedTokenBlacklistView(TokenBlacklistView):
    @swagger_auto_schema(
        responses={
            status.HTTP_200_OK: TokenBlacklistResponseSerializer,
        }
    )
    def post(self, request, *args, **kwargs):
        return super().post(request, *args, **kwargs)
